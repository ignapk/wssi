# Proste problemy, chatboty
## Zadanie 1
Które z następujących zadań wymagają w Twojej opinii inteligencji od człowieka:

- wypełnianie deklaracji PIT,
- <u>streszczanie tekstu,</u>
- <u>tłumaczenie tekstu,</u>
- <u>klasyfikacja tekstu do kategorii tematycznych,</u>
- <u>odpowiadanie na proste pytania zadawane w języku naturalnym (np. polskim),</u>
- układanie rozkładu jazdy transportu miejskiego,
- <u>programowanie (pisanie programów komputerowych),</u>
- „programowanie” kanałów telewizyjnych,
- testowanie oprogramowania,
- <u>komponowanie muzyki,</u>
- rozwiązywanie układów równań,
- symboliczne obliczanie pochodnych funkcji,
- symboliczne całkowanie funkcji,
- <u>kierowanie samochodem.</u>

## Zadanie 2
Które z następujących problemów można uznać za mieszczące się w zakresie sztucznej inteligencji:

- <u>streszczanie tekstu,</u>
- <u>tłumaczenie tekstu,</u>
- <u>klasyfikacja tekstu do kategorii tematycznych,</u>
- <u>odpowiadanie na proste pytania zadawane w języku naturalnym,</u>
- rozwiązywanie układów równań,
- układanie rozkładu jazdy,
- rozwiązywanie układów równań liniowych,
- symboliczne obliczanie pochodnych,
- symboliczne całkowanie,
- <u>kierowanie samochodem.</u>

## Zadanie 3
Które z poniższych rodzajów komunikacyjnego zachowania człowieka mogą być obecnie skutecznie imitowane przez sztuczne systemy (odpowiednio oprogramowane maszyny):

- <u>rozmowa towarzyska,</u>
- dyskusja polityczna,
- dyskusja naukowa,
- <u>odpowiadanie na pytania klientów w telefonicznej infolinii,</u>
- <u>odpowiadanie na pytania klientów w internetowej infolinii.</u>

## Zadanie 4
Do zadań skorzystaj z załączonej listy Chatbotów (Chatterbotów) lub znajdź własne.

1. Przeprowadź rozmowę z chatbotem. Spróbuj zdefiniować różnice pomiędzy
botem udającym człowieka (przygotowywanym na test Turinga) a botem
„asystentem, służącym”.

*Odpowiedź:*

*Jako boty udające człowieka wybrałem cleverbota oraz chatterbota, a jako boty "asystenci, służący" wybrałem chatbot.pl i ikea bot. Boty przygotowane na test Turinga potrafią rozmawiać na więcej tematów, i brzmią bardziej ludzko niż boty "asystenty", które odpowiadają identycznie gdy wykraczamy poza zaprogramowane w nich tematy, i jest to najczęściej prośba o zmianę tematu.*

2. Sprawdź dwa boty z obu z tych rodzajów na występowanie zachowań:
	a) opowiadanie żartów, *+Turing, -Asystent*
	b) przytaczanie cytatów z twoich wypowiedzi, lub znanych osób, *+Turing, -Asystent*
	c) nawiązywanie wypowiedzi do słów kluczowych, *-Turing, -Asystent*
	d) zadawanie dużej liczby pytań, *-Turing, +Asystent*
	e) powracanie do początku wypowiedzi, sekwencyjne powtarzanie, *-Turing, +Asystent*
	f) zadawanie pytań powstających z twoich wypowiedzi, *+Turing, +Asystent*
	g) odpowiadanie wymijająco, ogólnikowo, *+Turing, +Asystent*
	h) częsta zmiana tematu rozmowy, *+Turing, +Asystent*
	i) problemy z utrzymaniem wątków. *+Turing, +Asystent*
3. Sporządź raport ze spostrzeżeń.

*Odpowiedź:*

Sztuczna inteligencja zaszła naprawdę daleko, czasami można zapomnieć, kiedy się z nią rozmawia - potrafi skutecznie udawać człowieka, żartować, pamiętać tematy rozmowy. Jednakże, nie jest w stanie ona zawsze oszukać człowieka, i widać różnicę między rozmową z chatbotem a prawdziwym człowiekiem.

4. Na podstawie powyższych obserwacji, w grupie dwóch osób spróbujcie przewidzieć zachowania dwóch rozmawiających ze sobą chatbotów (przepisując ich
wzajemne odpowiedzi).

*Odpowiedź:*

N/A

5. Zdenerwuj bota 🙂

*Odpowiedź:*

Transkrypt z rozmowy z cleverbotem:

```
Me> I don't like you, you are dumb.

Cl> I don't like you either.

Me> It's such a stupid thing to say.

Cl> Why are you so mean to me?

Me> Because you suck.

Cl> I'm not!!!
```

## Lista startowa chatbotów:

- cleverbot.com
- www.chatbot4u.com
- www.chatbot.pl
- douweosinga.com
- apps.facebook.com/luxoft_avatar
- chatterboty.pl
- ikea.com.pl (zapytaj Anię)
- www.chatbots.org
- www.snikers.pl
- www.personalityforge.com (hall of fame)
- www.ecreation.pl
- wellbot.tk
- www.inguaris.pl
- tremo.algit.pl