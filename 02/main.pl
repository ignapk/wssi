lubi(wiktoria, dominika).
lubi(dominika, wiktoria).
lubi(jan, pawel).
lubi(pawel, julia).
lubi(pawel, jan).
lubi(jan, bartek).
lubi(bartek, agata).
lubi(agata, bartek).
lubi(michal, kichal).
lubi(kichal, michal).

kobieta(julia).
kobieta(agata).
kobieta(wiktoria).
kobieta(dominika).

mezczyzn(X) :- \+ kobieta(X).

przyjazn(X, Y) :- lubi(X,Y), lubi(Y,X).

niby_przyjazn(X,Y) :- lubi(X,Y); lubi(Y,X).

nieprzyjazn(X,Y) :- \+przyjazn(X,Y).

loves(X, Y) :- lubi(X, Y), \+ (lubi(X,Z), Z \= Y).

true_loves(X,Y) :- przyjazn(X,Y), \+ ((niby_przyjazn(X, Z); niby_przyjazn(Y, Z)), Z \= X, Z \= Y).
